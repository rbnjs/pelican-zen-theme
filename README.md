# Pelican Zen

Simple pelican theme optimized for reading.

## Installation

1. [Install Pelican and create a project](https://docs.getpelican.com/en/latest/quickstart.html)
2. Clone the project
3. [Configure your project to use pelican-zen](https://docs.getpelican.com/en/latest/settings.html#themes)

A sample configuration is below.

## Configuration

The configuration is mostly based on the base pelican theme.

```python
#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Ruben Serradas'
SITENAME = 'Serradas.org'
SITEURL = 'http://127.0.0.1:8083'
SITE_DESCRIPTION = 'Ruben\'s personal website. Ideas and projects.'
SITE_WELCOME = """
<h2>Hi, I'm Ruben</h2>
<p>Welcome! I write ideas and software.</p>
"""
SITE_FOOTER = """
<p xmlns:cc="http://creativecommons.org/ns#" >This work is licensed under <a
href="http://creativecommons.org/licenses/by-nc/4.0/?ref=chooser-v1"
target="_blank" rel="license noopener noreferrer"
style="display:inline-block;">CC BY-NC 4.0</a></p>
"""

PATH = 'content'

TIMEZONE = 'Europe/Berlin'

DEFAULT_LANG = 'en'
LOCALE = 'en_US'
DEFAULT_DATE_FORMAT = '%b %Y'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME = '/home/path/to/your/theme'

# Blogroll
MENUITEMS = (('Reading', 'https://www.goodreads.com/rubenserradas'),
             ('Code', 'https://github.com/RubenSerradas/'),
             ('RSS', 'https://serradas.org/feeds/all.atom.xml')
         )

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 3

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

SITEMAP = {
    "format": "xml",
    "priorities": {
        "articles": 0.5,
        "indexes": 0.5,
        "pages": 0.5
    },
    "changefreqs": {
        "articles": "monthly",
        "indexes": "daily",
        "pages": "monthly"
    }
}

PORT = 8083

EXTRA_PATH_METADATA = {
    'extra/favicon.ico': {'path': 'favicon.ico'},
    'extra/CNAME': {'path': 'CNAME'},
}

# static paths will be copied without parsing their contents
STATIC_PATHS = [
        'images',
        'extra',
        ]
```

## Contribute

We use [C4](https://rfc.zeromq.org/spec/42/).

### Development

You'll need [nodejs](https://nodejs.org/en/) to run node-sass. To install the project run:

```
npm i
```

Once installed. You can change `sass/styles.scss` and see the changes in the theme when you run:

```
npm start
```

When you run `npm start` you'll fire a watcher for that file. So you don't have to worry later when you're changing stuff.


## License

[GPL 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
